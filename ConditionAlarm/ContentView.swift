//
//  ContentView.swift
//  ConditionAlarm
//
//  Created by Klaus Schleicher14 on 06.11.19.
//  Copyright © 2019 KApps. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("New Version of the WeatherConditionAlarm")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
