//
//  Planning.swift
//  ConditionAlarm
//
//  Created by Klaus Schleicher14 on 08.11.19.
//  Copyright © 2019 KApps. All rights reserved.
//

import SwiftUI

struct Planning:Hashable, Codable, Identifiable {
    var id:Int
    var name:String
    var image:String
    var thumb:String
}
    
