//
//  PlanningItem.swift
//  ConditionAlarm
//
//  Created by Klaus Schleicher14 on 08.11.19.
//  Copyright © 2019 KApps. All rights reserved.
//

import SwiftUI

struct PlanningItem: View {
    
    var planning: Planning
    
    var body: some View {
        Image(planning.image)
        .resizable()
        .aspectRatio(contentMode: .fill)
        .frame(width: 300, height: 200)
        
        
    }
}

struct PlanningItem_Previews: PreviewProvider {
    static var previews: some View {
        PlanningItem(planning: planningData[0])
    }
}
